import { makeStyles } from '@material-ui/core/styles';
const styles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.main,
  },
}));
export default styles;
