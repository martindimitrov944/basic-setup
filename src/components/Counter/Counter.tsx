import { useSelector, useDispatch } from 'hooks/redux';
import { useState, useEffect } from 'react';
import { decrement, increment, count } from 'store/slices/counter';
import styles from './CounterStyles';
import { RootState } from 'store';
const classes = styles();
const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state: RootState) => state.counter);
  console.log(counter);
  console.log(counter.value);
  const [state, setState] = useState(0);
  for (let i = 0; i < 100; i++) {
    setState(Math.random());
  }

  return (
    <div>
      <>
        <button className={classes.root} onClick={() => dispatch(increment())}>
          increase
        </button>
        <button onClick={() => dispatch(decrement())}>decrease</button>
        <button onClick={() => dispatch(count())}>saga</button>
      </>
    </div>
  );
};
export default Counter;
