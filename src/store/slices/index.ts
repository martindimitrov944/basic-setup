import counterSlice from './counter';

const reducer = {
  counter: counterSlice,
};
export default reducer;
