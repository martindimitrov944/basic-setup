import { put, call, takeLatest } from 'redux-saga/effects';
import { actions } from 'store/slices/counter';

function* handleCount() {
  console.log('here2');
  const test = { name: 'smth', age: 5 };
  yield put(actions.increment());
  yield put(actions.smth(test));
}

export default function* counterWatcher() {
  yield takeLatest(actions.count.type, handleCount);
}
