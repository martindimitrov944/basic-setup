import { all, fork } from 'redux-saga/effects';
import counterWatcher from './counter';

export default function* rootSaga() {
  yield all([fork(counterWatcher)]);
}
