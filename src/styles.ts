import { createTheme } from '@mui/material/styles';

// const theme = createTheme({
//   status: {
//     danger: '#e53e3e',
//   },
//   palette: {
//     common: {
//       main: '#64748B',
//       contrastText: '#fff',
//       test: 'green',
//     },
//   },
// });
const theme = createTheme({
  status: {
    danger: '#e53e3e',
  },
  palette: {
    primary: {
      main: 'green',
    },
    neutral: {
      main: '#64748B',
      contrastText: '#fff',
    },
  },
});
export default theme;
