import React from 'react';
import Counter from 'components/Counter';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from 'styles';
import './App.css';
function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Counter />
      </ThemeProvider>
    </div>
  );
}

export default App;
